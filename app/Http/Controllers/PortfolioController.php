<?php

namespace App\Http\Controllers;

class PortfolioController extends Controller
{
    public function home() {
        return view('portfolio.home'); 
    }

    public function about() {
        $data = json_decode(file_get_contents(storage_path('data/portfolio/about.json')));

        return view('portfolio.about', compact('data'));
    }

    public function workExperience() {
        $data = json_decode(file_get_contents(storage_path('data/portfolio/work_experience.json')));

        return view('portfolio.work_experience', compact('data'));
    }

    public function projects() {
        $data = json_decode(file_get_contents(storage_path('data/portfolio/projects.json')));

        return view('portfolio.projects', compact('data'));
    }

    public function projectDetail($id) {
       
        $projects = json_decode(file_get_contents(storage_path('data/portfolio/projects.json')));

        $projectDetail = null;

        foreach ($projects as $project) {
            
            if($project->id === (int) $id){
                $projectDetail = $project;
                break;
            }
        }
        
        return view('portfolio.project_detail', compact('projectDetail'));

    }
}
