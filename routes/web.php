<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PortfolioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('portfolio.home')->name('portfolio.home');
// });

Route::get('/', [PortfolioController::class, 'home'])->name('portfolio.home');
Route::get('/about', [PortfolioController::class, 'about'])->name('portfolio.about');
Route::get('/work-experience', [PortfolioController::class, 'workExperience'])->name('portfolio.work_experience');
Route::get('/projects', [PortfolioController::class, 'projects'])->name('portfolio.projects');
Route::get('/project-detail/{id}', [PortfolioController::class, 'projectDetail'])->name('portfolio.project_detail');


